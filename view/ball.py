from PyQt5.QtWidgets import QWidget, QPushButton

COLOR = "#b50010"
SIZE = 100


class Ball(QWidget):
    def __init__(self):
        super().__init__()
        self.child = QPushButton(self)
        self.child.setStyleSheet(("background-color:" + COLOR + "; border-radius:50%;"))
        self.child.resize(SIZE, SIZE)
        self.child.move(750,500)
