from PyQt5.QtWidgets import QWidget, QVBoxLayout

COLOR = "#804180"


class Target(QWidget):
    def __init__(self, location):
        super().__init__()
        self.location = location
        self.target = QWidget()
        self.load()

    def load(self):
        child = QVBoxLayout(self)
        child.setContentsMargins(0,0,0,0)
        self.target.setStyleSheet("background-color:%s;" % COLOR)
        child.addWidget(self.target)

    def show(self):
        self.target.setStyleSheet("background-color:%s;" % COLOR)

    def hide(self):
        self.target.setStyleSheet("background-color:transparent;")