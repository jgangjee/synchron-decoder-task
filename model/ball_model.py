import json
import math
import os

import numpy as np
from PyQt5 import QtCore
from PyQt5.QtCore import QObject
from PyQt5.QtGui import QGuiApplication

JSON_PATH = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'resources', 'ball_params.json'))

with open(JSON_PATH) as file:
    data = json.load(file)


class BallModel(QObject):
    goSignal = QtCore.pyqtSignal()
    updateSignal = QtCore.pyqtSignal()

    def __init__(self):
        super().__init__()
        self.pos_freq = data['pos_freq']
        self.update_freq = data['update_freq']
        self.scaling_factor = data['scaling_factor']
        self.raw_data_size = data['sample_size']
        self.weight = data['weight']

        self.raw_data = []
        self.mean_array = []
        self.current_velocity = 0.0
        self.num_cycles = 0

    def reset(self):
        self.raw_data = []
        self.num_cycles = 0
        self.current_velocity = 0.0
        self.mean_array = []

    def get_offset_x(self):
        if len(self.raw_data) > 0:
            screen_width = QGuiApplication.primaryScreen().geometry().width()
            mean = self.get_geom_wma(self.raw_data)
            # Sampling Distribution of the Sample Mean
            self.insert_data(self.mean_array, mean)
            return self.get_geom_wma(self.mean_array)
        else:
            return 0

    def get_geom_wma(self, array):
        # Geometrically weighted moving average
        return np.ma.average(array, weights=np.geomspace(self.weight, 1, num=len(array)))

    def get_pos_x(self, center_x):
        self.num_cycles += 1
        # Pixel Distance = Scaled Velocity X Time
        px_distance = self.current_velocity * self.scaling_factor * self.num_cycles
        self.insert_data(self.raw_data, int(px_distance))
        offset_x = self.get_offset_x()
        return math.floor(center_x + offset_x)

    def insert_data(self, array, value):
        array.insert(0, value)
        if len(array) > self.raw_data_size:
            array.pop()
