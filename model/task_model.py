import json
import os
import random

from PyQt5 import QtCore
from PyQt5.QtCore import QObject

LEFT_TARGET = "L"
RIGHT_TARGET = "R"
S_TO_MS = 1000
JSON_PATH = os.path.realpath(os.path.join(os.path.dirname(__file__), '..', 'resources', 'task_params.json'))

with open(JSON_PATH) as file:
    data = json.load(file)


class TaskModel(QObject):
    restSignal = QtCore.pyqtSignal()
    leftSignal = QtCore.pyqtSignal()
    rightSignal = QtCore.pyqtSignal()

    def __init__(self):
        super().__init__()
        self.go_time = data['go_time'] * S_TO_MS
        self.rest_time = data['rest_time'] * S_TO_MS
        self.num_trials = data['num_trials']
        self.trial_order = self.get_trial_order()

    def get_trial_order(self):
        trial_order = []
        for i in range(self.num_trials):
            trial_order.append(LEFT_TARGET)
            trial_order.append(RIGHT_TARGET)
        random.shuffle(trial_order)
        return trial_order
