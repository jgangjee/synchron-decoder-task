import numpy as np
from PyQt5.QtCore import QObject


class DecoderModel(QObject):
    def __init__(self):
        super().__init__()
        self.v_min = -1
        self.v_max = 1

        self.decoder_array = []

    def generate_uniform_distribution(self, size: int):
        self.decoder_array = np.random.uniform(self.v_min, self.v_max, size)
        print("Generating random uniform distribution velocity values:" + str(self.decoder_array))

    def get_update_velocity(self, index: int):
        return self.decoder_array[index]
