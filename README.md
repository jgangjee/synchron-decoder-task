# Synchron Decoder Task

## Checklist
- Ball position updated at 25Hz
- Velocity generated randomly at 10Hz based on uniform distribution between -1 and 1
- Randomized trial order generated based on <code>num_trials</code> provided in <code>tasks_params.json</code>
- During rest, ball position is reset and no targets are shown
- Scaling Factor provided to fit larger dimensions

## Description
The ball position is calculated based on Sampling distribution of the Sample Mean with a Geometrically Weighted Moving Average.  
- **Sample Size**: More samples conveys big picture context, fewer samples show their more recent actions. 
- **Weight**: Controls the exponential curve. A steeper curve would be more accurate but may have a more choppy result, a flatter curve would provide a smoother experience but may not give enough weight to changing intention

While we can test a baseline that balances both these factors, sample size=25 (1 sec) and weight=5, it would be different for each user's profile and the overall purpose of the task. Power users may use a smaller sample size with a steeper weighted curve, while new users would need a larger sample size and a flatter curve.

(Change <code>ball_params.json</code> to play with these settings)

![A red ball in the middle of a blank background with a purple target to the left](/resources/Screenshot_1.png)

## Setup
Clone and run <code>python main.py</code>
_Note: The ball position is set based on the resolution of the primary screen_

