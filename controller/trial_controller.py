from PyQt5.QtTest import QTest

from model.ball_model import BallModel
from model.task_model import TaskModel

UPDATE_FREQUENCY = 10
S_TO_MS = 1000


class TrialController:

    def __init__(self, task_model: TaskModel, ball_model: BallModel):
        self.task_model = task_model
        self.ball_model = ball_model
        self.index = 0

    def get_next(self):
        if self.index < len(self.task_model.trial_order):
            if self.task_model.trial_order[self.index] == 'L':
                self.left()
            else:
                self.right()
        else:
            print('Done')

    def right(self):
        print('Go Right')
        self.ball_model.goSignal.emit()
        self.ball_model.updateSignal.emit()
        self.task_model.rightSignal.emit()
        QTest.qWait(self.task_model.go_time)
        self.rest()

    def left(self):
        print('Go Left')
        self.ball_model.goSignal.emit()
        self.ball_model.updateSignal.emit()
        self.task_model.leftSignal.emit()
        QTest.qWait(self.task_model.go_time)
        self.rest()

    def rest(self):
        print('Rest')
        self.task_model.restSignal.emit()
        self.index += 1
        QTest.qWait(self.task_model.rest_time)
        self.get_next()

    def start_task(self):
        print("Starting task with trial order:" + self.task_model.trial_order.__str__())
        self.get_next()
