from PyQt5.QtCore import QTimer
from PyQt5.QtGui import QGuiApplication

from model.ball_model import BallModel
from model.decoder_model import DecoderModel
from model.task_model import TaskModel
from view.ball import Ball

EDGE_PADDING = 50
S_TO_MS = 1000


class BallController:
    def __init__(self, ball_view: Ball, task_model: TaskModel, ball_model: BallModel):
        self.ball_view = ball_view

        self.go_timer = QTimer()
        self.update_timer = QTimer()

        self.task_model = task_model
        self.task_model.restSignal.connect(self.rest)
        self.index = 0

        self.ball_model = ball_model
        self.ball_model.goSignal.connect(self.go)
        self.ball_model.updateSignal.connect(self.update)

        self.decoder_model = DecoderModel()

        # Couldn't find an easier pyqt function to get object location relative to the display
        screen = QGuiApplication.primaryScreen().geometry()
        self.height = screen.height()
        self.width = screen.width()
        self.target_width = self.width * (1 / 14)
        self.center_x = int(self.width / 2 - self.target_width - self.ball_view.child.width())

    def rest(self):
        self.ball_model.reset()
        self.end_timer(self.go_timer)
        self.end_timer(self.update_timer)
        self.move_ball(self.center_x)
        self.index = 0
        print("Bringing ball to rest")

    def end_timer(self, timer: QTimer):
        timer.stop()
        timer.disconnect()

    def go(self):
        print("Updating ball position")
        self.go_timer.timeout.connect(self.update_ball_pos)
        self.go_timer.start(self.get_go_cycle_time())

    def get_go_cycle_time(self):
        return int(S_TO_MS * 1 / self.ball_model.pos_freq)

    def update(self):
        print("Updating velocity")
        self.decoder_model.generate_uniform_distribution(self.task_model.go_time)
        self.update_timer.timeout.connect(self.update_velocity)
        self.update_timer.start(self.get_update_cycle_time())

    def get_update_cycle_time(self):
        return int(S_TO_MS * 1 / self.ball_model.update_freq)

    def update_velocity(self):
        self.index += 1
        update_velocity = self.decoder_model.get_update_velocity(self.index)
        self.ball_model.current_velocity = update_velocity

    def update_ball_pos(self):
        pos_x = self.ball_model.get_pos_x(self.center_x)
        self.move_ball(pos_x)

    def move_ball(self, x):
        x = max(0, min(x, int(self.width - self.target_width * 2 - self.ball_view.child.width() - EDGE_PADDING)))
        self.ball_view.child.move(x, int(self.height / 2) - self.ball_view.child.width())
