from model.task_model import TaskModel
from view.target import Target


class TargetController:

    def __init__(self, target_l: Target, target_r: Target, task_model: TaskModel):
        self._target_l = target_l
        self._target_r = target_r
        self.task_model = task_model
        self.connect_signals()

    def connect_signals(self):
        self.task_model.leftSignal.connect(self.left)
        self.task_model.rightSignal.connect(self.right)
        self.task_model.restSignal.connect(self.rest)

    def right(self):
        self._target_l.hide()
        self._target_r.show()

    def left(self):
        self._target_r.hide()
        self._target_l.show()

    def rest(self):
        self._target_l.hide()
        self._target_r.hide()
