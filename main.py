import sys

from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout

from controller.ball_controller import BallController
from controller.target_controller import TargetController
from controller.trial_controller import TrialController
from model.ball_model import BallModel
from model.task_model import TaskModel
from view.ball import Ball
from view.target import Target

app = QApplication(sys.argv)

window = QWidget()

layout = QHBoxLayout()
window.setLayout(layout)
window.layout().setContentsMargins(0, 0, 0, 0)

target_l = Target('L')
window.layout().addWidget(target_l, 1)

ball = Ball()
window.layout().addWidget(ball, 12)

target_r = Target('R')
window.layout().addWidget(target_r, 1)

window.showFullScreen()

task_model = TaskModel()
ball_model = BallModel()

trial_controller = TrialController(task_model, ball_model)
target_controller = TargetController(target_l, target_r, task_model)

ball_controller = BallController(ball, task_model, ball_model)
trial_controller.start_task()
app.exec()
